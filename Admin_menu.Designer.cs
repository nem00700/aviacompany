﻿namespace aviacompany
{
    partial class Admin_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.авикомпанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.рейсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.аэропортToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.администраторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.забронироватьБилетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.авикомпанияToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.рейсToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.аэропортToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.пассажирToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пользовательToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.забронироватьБилетToolStripMenuItem,
            this.выходToolStripMenuItem,
            this.удалитьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(469, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.авикомпанияToolStripMenuItem,
            this.рейсToolStripMenuItem,
            this.аэропортToolStripMenuItem,
            this.администраторToolStripMenuItem});
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            // 
            // авикомпанияToolStripMenuItem
            // 
            this.авикомпанияToolStripMenuItem.Name = "авикомпанияToolStripMenuItem";
            this.авикомпанияToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.авикомпанияToolStripMenuItem.Text = "Авикомпания";
            this.авикомпанияToolStripMenuItem.Click += new System.EventHandler(this.авикомпанияToolStripMenuItem_Click);
            // 
            // рейсToolStripMenuItem
            // 
            this.рейсToolStripMenuItem.Name = "рейсToolStripMenuItem";
            this.рейсToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.рейсToolStripMenuItem.Text = "Рейс";
            this.рейсToolStripMenuItem.Click += new System.EventHandler(this.рейсToolStripMenuItem_Click_1);
            // 
            // аэропортToolStripMenuItem
            // 
            this.аэропортToolStripMenuItem.Name = "аэропортToolStripMenuItem";
            this.аэропортToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.аэропортToolStripMenuItem.Text = "Аэропорт";
            this.аэропортToolStripMenuItem.Click += new System.EventHandler(this.аэропортToolStripMenuItem_Click_1);
            // 
            // администраторToolStripMenuItem
            // 
            this.администраторToolStripMenuItem.Name = "администраторToolStripMenuItem";
            this.администраторToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.администраторToolStripMenuItem.Text = "Администратор";
            this.администраторToolStripMenuItem.Click += new System.EventHandler(this.администраторToolStripMenuItem_Click);
            // 
            // забронироватьБилетToolStripMenuItem
            // 
            this.забронироватьБилетToolStripMenuItem.Name = "забронироватьБилетToolStripMenuItem";
            this.забронироватьБилетToolStripMenuItem.Size = new System.Drawing.Size(139, 20);
            this.забронироватьБилетToolStripMenuItem.Text = "Забронировать билет";
            this.забронироватьБилетToolStripMenuItem.Click += new System.EventHandler(this.забронироватьБилетToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.авикомпанияToolStripMenuItem1,
            this.рейсToolStripMenuItem1,
            this.аэропортToolStripMenuItem1,
            this.пассажирToolStripMenuItem,
            this.пользовательToolStripMenuItem});
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            // 
            // авикомпанияToolStripMenuItem1
            // 
            this.авикомпанияToolStripMenuItem1.Name = "авикомпанияToolStripMenuItem1";
            this.авикомпанияToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.авикомпанияToolStripMenuItem1.Text = "Авикомпания";
            this.авикомпанияToolStripMenuItem1.Click += new System.EventHandler(this.авикомпанияToolStripMenuItem1_Click);
            // 
            // рейсToolStripMenuItem1
            // 
            this.рейсToolStripMenuItem1.Name = "рейсToolStripMenuItem1";
            this.рейсToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.рейсToolStripMenuItem1.Text = "Рейс";
            this.рейсToolStripMenuItem1.Click += new System.EventHandler(this.рейсToolStripMenuItem1_Click);
            // 
            // аэропортToolStripMenuItem1
            // 
            this.аэропортToolStripMenuItem1.Name = "аэропортToolStripMenuItem1";
            this.аэропортToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.аэропортToolStripMenuItem1.Text = "Аэропорт";
            this.аэропортToolStripMenuItem1.Click += new System.EventHandler(this.аэропортToolStripMenuItem1_Click);
            // 
            // пассажирToolStripMenuItem
            // 
            this.пассажирToolStripMenuItem.Name = "пассажирToolStripMenuItem";
            this.пассажирToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.пассажирToolStripMenuItem.Text = "Пассажир";
            this.пассажирToolStripMenuItem.Click += new System.EventHandler(this.пассажирToolStripMenuItem_Click);
            // 
            // пользовательToolStripMenuItem
            // 
            this.пользовательToolStripMenuItem.Name = "пользовательToolStripMenuItem";
            this.пользовательToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.пользовательToolStripMenuItem.Text = "Пользователь";
            this.пользовательToolStripMenuItem.Click += new System.EventHandler(this.пользовательToolStripMenuItem_Click);
            // 
            // Admin_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Admin_menu";
            this.Text = "Admin_menu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem авикомпанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рейсToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem аэропортToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem администраторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem забронироватьБилетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem авикомпанияToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem рейсToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem аэропортToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem пассажирToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пользовательToolStripMenuItem;
    }
}