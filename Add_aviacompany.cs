﻿using System;
using Npgsql;
using DbApi;
using System.Data.SqlClient;
using NpgsqlTypes;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;

namespace aviacompany
{
    
    public partial class Add_aviacompany : Form
    {
        private MyApi myApi1 = null;
        public Add_aviacompany(NpgsqlConnection conn)
        {
            myApi1 = new MyApi();
            myApi1.SetConnection(conn);
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(myApi1.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            String name = textBox1.Text.ToString();
            String address = textBox2.Text.ToString();
            String phone = textBox3.Text.ToString();



            bool res = myApi1.Insert("airlines", new String[] { "name", "address", "phone" },
                          new String[] { name, address, phone });
            if (res)
            {
                MessageBox.Show("Новая авиакомпания успешно добавлена!");
            }
            else
                MessageBox.Show("Данные о новой авиакомпании содержат ошибки и не могут быть добавлены!");


        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
