﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace aviacompany
{
    public partial class User_login_or_administrator : Form
    {
        private MyApi api = null;
        public User_login_or_administrator(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();
            
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            this.Visible = false;
            frm.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String e_mail = textBox1.Text.ToString();
            String password = textBox2.Text.ToString();
            String role_box = comboBox1.Text.ToString();
            
            if ((Convert.ToString(api.SelectSql("SELECT count(*) FROM users WHERE e_mail = @id0", new Object[] { e_mail })[0][0])) != "0"){
                var password_table = Convert.ToString(api.SelectSql("SELECT password, role FROM users WHERE e_mail = @id0", new Object[] { e_mail })[0][0]);

                var role = Convert.ToString(api.SelectSql("SELECT password, role FROM users WHERE e_mail = @id0", new Object[] { e_mail })[0][1]);

                var id_user = Convert.ToInt32(api.SelectSql("SELECT password, role, id_user FROM users WHERE e_mail = @id0", new Object[] { e_mail })[0][2]);

                bool res = api.Insert("sessions", new String[] { "id_user"},
                          new Object[] { id_user});

                if (role_box == "Администратор")
                {
                    if ((password_table.Equals(password)) && (role == "True"))
                    {
                        Data.Value = Convert.ToString(api.SelectSql("SELECT name FROM users WHERE e_mail = @id0", new Object[] { e_mail })[0][0]);
                        
                        Data.Value2 = Convert.ToString(api.SelectSql("SELECT surname FROM users WHERE e_mail = @id0", new Object[] { e_mail })[0][0]);
                       
                        Admin_menu frm = new Admin_menu(api.GetConnection());
                        this.Visible = false;
                        frm.Show();
                    }
                    else if ((password_table.Equals(password)) && (role != "True"))
                    {
                        MessageBox.Show("Данный пользователь не является администратором.");
                    }
                    else
                    {
                        MessageBox.Show("Введенные данные неверны, попробуйте снова.");
                    }
                }
                else if (role_box == "Пользователь")
                {
                    if (password_table.Equals(password))
                    {
                        User_menu frm = new User_menu(api.GetConnection());
                        this.Visible = false;
                        frm.Show();
                    }
                    else
                    {
                        MessageBox.Show("Введенные данные неверны, попробуйте снова.");
                    }


                }
            }
            else
                MessageBox.Show("Пользователя с таким е-mail не существует.");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
