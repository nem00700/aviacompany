﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Admin_menu : Form
    {
        private MyApi api = null;
        public Admin_menu(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();
        }
        

        private void забронироватьБилетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Buy_ticket frm = new Buy_ticket(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            this.Visible = false;
            frm.Show();
        }

        private void авикомпанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_aviacompany frm = new Add_aviacompany(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void рейсToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Add_flight frm = new Add_flight(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void аэропортToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Add_airport frm = new Add_airport(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void администраторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_admin frm = new Add_admin(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void авикомпанияToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Delete_airlines frm = new Delete_airlines(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void рейсToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Delete_flight frm = new Delete_flight(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void аэропортToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Delete_airport frm = new Delete_airport(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void пассажирToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Delete_passenger frm = new Delete_passenger(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void пользовательToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Delete_user frm = new Delete_user(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        /*
        private void войтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserRegistration frm = new UserRegistration(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void пользовательToolStripMenuItem_Click(object sender, EventArgs e)
        {
            User_login_or_administrator frm = new User_login_or_administrator();
            this.Visible = false;
            frm.Show();
        }

        private void администраторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            User_login_or_administrator frm = new User_login_or_administrator();
            this.Visible = false;
            frm.Show();
        }

        private void гостьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Guest_login frm = new Guest_login();
            this.Visible = false;
            frm.Show();
        }

        private void купитьБилетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Buy_ticket frm = new Buy_ticket(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }


        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            api.Close();
        }
        */
    }
}
