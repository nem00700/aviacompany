﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace DbApi
{
    interface IMyAPI
    {
        bool Connection(String connectionLine);
        bool Close();
        bool CreateTableSql(String sql);
        bool DropTableSql(String sql);
        bool InsertSql(String sql);
        bool Insert(String tableName, String[] fields, Object[] values);
        bool UpdateSql(String sql);
        bool Update(String tableName, String[] fields, String[] values, String select);
        List<List<object>> SelectSql(String sql);
        List<List<object>> Select(String[] tablesNames, String[] fields);
       // Dictionary<string, string> Select();
    }
}
