﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbApi;
using aviacompany;



namespace ApiTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConnectCloseMethods()
        {
            MyApi api = new MyApi();
            if (!api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany"))
            {
                Assert.Fail("Не удается подключиться к базе");
            }

            if (!api.Close())
            {
                Assert.Fail("Не удается отключиться от базы");
            }

        }

        [TestMethod]
        public void TestCreateAndDropSqlTableMethods()
        {
            MyApi api = new MyApi();
            if (!api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany"))
            {
                Assert.Fail("Не удается подключиться к базе");
            }

            var resultCreate = api.CreateTableSql("CREATE TABLE tests (id bigserial primary key, name text NOT NULL," +
            "address text NOT NULL, phone text NOT NULL)");
            if (resultCreate == false)
            {
                api.Close();
                Assert.Fail("Create не работает");
            }
            else
            {
                var resultDrop = api.DropTableSql("DROP TABLE IF EXISTS tests");
                api.Close();
                if (resultDrop == false)
                {
                    Assert.Fail("Drop не работает");
                }
            }
        }

        [TestMethod]
        public void TestSelectSqlMethod()
        {
            MyApi api = new MyApi();
            api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany");

            var resultCreate = api.CreateTableSql("CREATE TABLE testsSelectSql (id bigserial primary key, name text NOT NULL," +
           "address text NOT NULL, phone text NOT NULL)");

            api.InsertSql("INSERT INTO testsSelectSql (name, address, phone) VALUES " +
                "('Аэрофлот1', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");
            api.InsertSql("INSERT INTO testsSelectSql (name, address, phone) VALUES " +
                "('Аэрофлот2', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");

            var items = api.Select(new String[] { "testsSelectSql" }, new String[] { "name", "address", "phone" });

            api.DropTableSql("DROP TABLE IF EXISTS testsSelectSql");
            api.Close();
            if (items == null)
            {
                Assert.Fail("Select не работает");
            }
        }

        [TestMethod]
        public void TestSelectMethod()
        {
            MyApi api = new MyApi();
            api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany");

            var resultCreate = api.CreateTableSql("CREATE TABLE testsSelect (id bigserial primary key, name text NOT NULL," +
           "address text NOT NULL, phone text NOT NULL)");

            api.InsertSql("INSERT INTO testsSelect (name, address, phone) VALUES " +
                "('Аэрофлот1', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");
            api.InsertSql("INSERT INTO testsSelect (name, address, phone) VALUES " +
                "('Аэрофлот2', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");

            var items = api.Select(new String[] { "testsSelect" }, new String[] { "name", "address", "phone" });

            api.DropTableSql("DROP TABLE IF EXISTS testsSelect");

            api.Close();

            if (items == null)
            {
                Assert.Fail("Select не работает");
            }
        }

        [TestMethod]
        public void TestInsertsMethods()
        {
            MyApi api = new MyApi();
            api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany");

            var resultCreate = api.CreateTableSql("CREATE TABLE testsInsert (id bigserial primary key, name text NOT NULL," +
           "address text NOT NULL, phone text NOT NULL)");

            var resultInsertSql = api.InsertSql("INSERT INTO testsInsert (name, address, phone) VALUES " +
                "('Аэрофлот1', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");

            var resultInsert = api.Insert("testsInsert", new String[] { "name", "address", "phone" },
                new String[] { "'Аэрофлот2'", "'119002, Россия, г. Москва, ул. Арбат, д. 10'", "'+7 800 444 55 55'" });

            api.DropTableSql("DROP TABLE IF EXISTS testsInsert");
            api.Close();

            if (!resultInsert)
            {
                Assert.Fail("Insert не работает");
            }
            if (!resultInsertSql)
            {
                Assert.Fail("InsertSQL не работает");
            }
        }

        [TestMethod]
        public void TestUpdatesMethods()
        {
            MyApi api = new MyApi();
            api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany");

            var resultCreate = api.CreateTableSql("CREATE TABLE testsUpdate (id bigserial primary key, name text NOT NULL," +
           "address text NOT NULL, phone text NOT NULL)");

            api.InsertSql("INSERT INTO testsUpdate (name, address, phone) VALUES " +
                "('Аэрофлот1', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");

            api.Insert("testsUpdate", new String[] { "name", "address", "phone" },
                new String[] { "'Аэрофлот2'", "'119002, Россия, г. Москва, ул. Арбат, д. 10'", "'+7 800 444 55 55'" });

            var resultUpdate = api.Update("testsUpdate", new String[] { "name" }, new String[] { "'Update1'" }, "name = 'Аэрофлот1'");
            var resultUpdateSql = api.UpdateSql("UPDATE testsUpdate SET (name) = ('Update2') WHERE name = 'Аэрофлот2'");

            api.DropTableSql("DROP TABLE IF EXISTS testsUpdate");
            api.Close();

            if (!resultUpdate)
            {
                Assert.Fail("Update не работает");
            }
            if (!resultUpdateSql)
            {
                Assert.Fail("UpdateSQL не работает");
            }            
        }
    }
}
