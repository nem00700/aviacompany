﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace DbApi
{
    public class MyApi : IMyAPI
    {
        private NpgsqlConnection connection;
        private bool PerformTheOperationShouldUniversalDataBase(String sql)
        {
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionLine">Server=localhost;Port=5432;User ID=postgres;Password=1;Database=cyberforum;</param>
        /// <returns></returns>
        public bool Connection(String connectionLine)
        {
            try
            {
                connection = new NpgsqlConnection(connectionLine);
                connection.Open();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        public NpgsqlConnection GetConnection()
        {
            return connection;
        }

        public void SetConnection(NpgsqlConnection conn)
        {
            connection = conn;
        }

        /// <summary>
        /// Закрытие соединения с базой
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            try
            {
                connection.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Создание таблицы в базе данных по sql запросу
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool CreateTableSql(String sql)
        {
            return PerformTheOperationShouldUniversalDataBase(sql);
        }

        /// <summary>
        /// Удаление таблицы по sql запросу
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool DropTableSql(String sql)
        {
            return PerformTheOperationShouldUniversalDataBase(sql);
        }

        /// <summary>
        /// Вставка данных в таблицу по sql запросу
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool InsertSql(String sql)
        {
            return PerformTheOperationShouldUniversalDataBase(sql);
        }

        /// <summary>
        /// Обновление данных в таблице по sql запросу
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool UpdateSql(String sql)
        {
            return PerformTheOperationShouldUniversalDataBase(sql);
        }

        /// <summary>
        /// Обновление данных в таблице с формированием sql внутри метода
        /// </summary>
        /// <param name="tableName"> название таблицы </param>
        /// <param name="fields"> массив полей </param>
        /// <param name="values"> массив значений </param>
        /// <param name="select"> условие выборки записи для обновления </param>
        /// <returns></returns>
        public bool Update(String tableName, String[] fields, String[] values, String select)
        {
            if (fields.Length == 0 || values.Length == 0)
                return false;

            String fieldsLine = fields.GetValue(0).ToString();
            for (int i = 1; i < fields.Length; ++i)
            {
                fieldsLine += ", " + fields.GetValue(i).ToString();
            }

            String valuesLine = values.GetValue(0).ToString();
            for (int i = 1; i < values.Length; ++i)
            {
                valuesLine += ", " + values.GetValue(i).ToString();
            }
            //UPDATE weather SET (temp_lo, temp_hi, prcp) = (temp_lo+1, temp_lo+15, DEFAULT)
            // WHERE city = 'San Francisco' AND date = '2003-07-03';
            String sql = "UPDATE " + tableName + " SET (" + fieldsLine + ") = (" + valuesLine + ") WHERE " + select;
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Вставка данных в табицу с формированием sql внутри метода
        /// </summary>
        /// <param name="tableName"> название таблицы </param>
        /// <param name="fields"> массив полей </param>
        /// <param name="values"> массив значений </param>
        /// <returns></returns>
        public bool Insert(String tableName, String[] fields, Object[] values)
        {
            if (fields.Length == 0 || values.Length == 0)
                return false;

            for (int i = 1; i < fields.Length; ++i)
            {
                if (values.GetValue(i).ToString() == "")
                    return false;
            }

            String fieldsLine = fields.GetValue(0).ToString();
            for (int i = 1; i < fields.Length; ++i)
            {
                fieldsLine += ", " + fields.GetValue(i).ToString();
            }

            String valuesLine = "";
            for (int i = 0; i < values.Length; ++i)
            {
                if (i == 0)
                {
                    valuesLine += "@id" + i.ToString();
                }
                else
                    valuesLine += ", @id" + i.ToString();
            }

            String sql = "INSERT INTO " + tableName + " (" + fieldsLine + ") VALUES (" + valuesLine +")";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);

                for (int i = 0; i < values.Length; ++i)
                { 
                    NpgsqlParameter p = new NpgsqlParameter("@id" + i.ToString(), values.GetValue(i));
                    cmd.Parameters.Add(p);
                }
                if(cmd.ExecuteNonQuery() == -1)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                
                return false;
            }
            return true;
        }

        /// <summary>
        /// Построение выборки из таблицы по sql запросу
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<List<object>> SelectSql(String sql)
        {
            List<List<object>> itemsList = new List<List<object>>();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                int size = reader.FieldCount;
                while (reader.Read())
                {
                    List<object> items = new List<object>();
                    for (int i = 0; i < size; ++i)
                    {
                        items.Add(reader[i]);
                    }
                    itemsList.Add(items);
                }
                reader.Close();
            }
            catch (Exception)
            {
                return null;
            }
            return itemsList;
        }

        /// <summary>
        /// Построение выборки из таблицы по sql запросу c передачей параметров
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="options">Что будет подставлено вместо параметров "@id"</param>
        /// <returns></returns>
        public List<List<object>> SelectSql(String sql, Object[] options)
        {
            List<List<object>> itemsList = new List<List<object>>();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                for (int i = 0; i < options.Length; ++i)
                {
                    NpgsqlParameter p = new NpgsqlParameter("@id" + i.ToString(), options.GetValue(i));
                    cmd.Parameters.Add(p);
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();
                int size = reader.FieldCount;
                while (reader.Read())
                {
                    List<object> items = new List<object>();
                    for (int i = 0; i < size; ++i)
                    {
                        items.Add(reader[i]);
                    }
                    itemsList.Add(items);
                }
                reader.Close();
            }
            catch (Exception)
            {
                return null;
            }
            return itemsList;
        }

        /// <summary>
        /// Построение выборки с формированием sql внутри метода
        /// </summary>
        /// <param name="tablesNames"> название таблицы </param>
        /// <param name="fields"> название полей для выборки </param>
        /// <returns></returns>
        public List<List<object>> Select(String[] tablesNames, String[] fields)
        {
            if (tablesNames.Length == 0 || fields.Length == 0)
                return null;

            String tablesNamesLine = tablesNames.GetValue(0).ToString();
            for (int i = 1; i < tablesNames.Length; ++i)
            {
                tablesNamesLine += ", " + tablesNames.GetValue(i).ToString();
            }

            String fieldsLine = fields.GetValue(0).ToString();
            for (int i = 1; i < fields.Length; ++i)
            {
                fieldsLine += ", " + fields.GetValue(i).ToString();
            }

            String sql = "SELECT " + fieldsLine + " FROM " + tablesNamesLine;
            List<List<object>> itemsList = new List<List<object>>();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                int size = reader.FieldCount;
                while (reader.Read())
                {
                    List<object> items = new List<object>();
                    for (int i = 0; i < size; ++i)
                    {
                        items.Add(reader[i]);
                    }
                    itemsList.Add(items);
                }
                reader.Close();
            }
            catch (Exception)
            {
                return null;
            }
            return itemsList;
        }

    }
}
