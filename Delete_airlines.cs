﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Delete_airlines : Form
    {
        private MyApi api = null;

        public Delete_airlines(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();

            var name_airline = api.Select(new String[] { "airlines" }, new String[] { "name"});

            for (int i = 0; i < name_airline.Count; i++)
            {
                comboBox1.Items.Add(name_airline[i][0]);
            }
        }
        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name_airlines = "'" + comboBox1.SelectedItem as String + "'";
           
            String id_airlines = Convert.ToString(api.SelectSql("SELECT id FROM airlines WHERE name = " + name_airlines)[0][0]);

            var res = api.SelectSql("DELETE FROM flight WHERE id = " + id_airlines);
            if (true)
            {
                MessageBox.Show("Авиакомпания успешно удалена!");
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
