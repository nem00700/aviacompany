﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DbApi;

namespace aviacompany
{
    public partial class Main : Form
    {
        private MyApi api = null;

        


        public Main()
        {
            InitializeComponent();
            api = new MyApi();
            api.Connection("Server=localhost;Port=5432;User ID=root;Password=root;Database=aviacompany");
            api.CreateTableSql("CREATE TABLE tests (id bigserial primary key, name text NOT NULL," +
            "address text NOT NULL, phone text NOT NULL)");
            
            var resultDrop = api.DropTableSql("DROP TABLE tests");
           //api.InsertSql("INSERT INTO airlines (name, address, phone) VALUES "+
           //     "('Аэрофлот1', '119002, Россия, г. Москва, ул. Арбат, д. 10', '+7 800 444 55 55')");
            
        }

 /*     

        

        
*/
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            api.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            User_login_or_administrator frm = new User_login_or_administrator(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Guest_menu frm = new Guest_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
