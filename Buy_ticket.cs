﻿using System;
using Npgsql;
using DbApi;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Buy_ticket : Form
    {
        private MyApi myApi1 = null;
        
        public Buy_ticket(NpgsqlConnection conn)
        {
            myApi1 = new MyApi();
            myApi1.SetConnection(conn);
            InitializeComponent();
            textBox1.Text = Data.Value;
            textBox2.Text = Data.Value2;


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(myApi1.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name = textBox1.Text.ToString();
            String surname = textBox2.Text.ToString();
            String patronymic = textBox3.Text.ToString();
            String passport = textBox4.Text.ToString();
            var birth_day = dateTimePicker1.Value.Date;
            bool sex;
            String gender = comboBox1.SelectedText.ToString();
            if (gender == "мужской")
                sex = true;
            else
                sex = false;

            String phone = textBox7.Text.ToString();



            bool res = myApi1.Insert("passenger", new String[] { "name", "surname", "patronymic", "passport", "birth_day", "sex", "phone", "count_visit" },
                          new Object[] { name, surname, patronymic, passport, birth_day, sex, phone, 1 });
            if (res)
            {
                MessageBox.Show("Новый пассажир успешно добавлен!");
            }
            else
                MessageBox.Show("Данные о новом пассажире содержат ошибки и не могут быть добавлены!");

            int id_passenger1 = Convert.ToInt32(myApi1.SelectSql("SELECT id_passenger FROM passenger WHERE passport = '" + passport + "'")[0][0]);

            Choose_flight frm = new Choose_flight(myApi1.GetConnection());
            frm.SetIdPassenger(id_passenger1);
            this.Visible = false;
            frm.Show();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
