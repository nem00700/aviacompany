﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{

    public partial class Add_airport : Form
    {

        private MyApi myApi1 = null;
        public Add_airport(NpgsqlConnection conn)
        {
            myApi1 = new MyApi();
            myApi1.SetConnection(conn);
            InitializeComponent();
        }

       private void Add_airport_Load(object sender, EventArgs e)
        {
           

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(myApi1.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name = textBox1.Text.ToString();
            String address = textBox2.Text.ToString();

            bool res = myApi1.Insert("airport", new String[] { "name", "address"},
                          new String[] { name, address});
            if (res)
            {
                MessageBox.Show("Новый аэропорт успешно добавлен!");
            }
            else
                MessageBox.Show("Данные о новом аэропорте содержат ошибки и не могут быть добавлены!");

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
