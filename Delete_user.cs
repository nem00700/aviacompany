﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using DbApi;
using Npgsql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Delete_user : Form
    {
        private MyApi api = null;

        public Delete_user(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();

            var name_user = api.Select(new String[] { "users" }, new String[] { "name", "surname", "id_user" });

            for (int i = 0; i < name_user.Count; i++)
            {
                comboBox1.Items.Add(name_user[i][0] + " " + name_user[i][1] + "(" + name_user[i][2] + ")");
            }
        }
        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name_user = comboBox1.SelectedItem as String;
            var name_user_table = api.Select(new String[] { "users" }, new String[] { "name", "surname", "id_user" });
            String id_user = "";
            for (int i = 0; i < name_user_table.Count; i++)
            {
                if ((name_user_table[i][0] + " " + name_user_table[i][1] + "(" + name_user_table[i][2] + ")") == name_user)
                    id_user = Convert.ToString(name_user_table[i][2]);

            }

            

            var res = api.SelectSql("DELETE FROM users WHERE id_user = " + id_user);
            if (true)
            {
                MessageBox.Show("Пользователь успешно удален!");
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
