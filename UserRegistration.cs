﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class UserRegistration : Form
    {
        private MyApi myApi = null;
        public UserRegistration(NpgsqlConnection conn)
        {
            myApi = new MyApi();
            myApi.SetConnection(conn);
            InitializeComponent();
        }
       

        private void button2_Click(object sender, EventArgs e)
        {
            Guest_menu frm = new Guest_menu(myApi.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name = textBox1.Text.ToString();
            String surname = textBox4.Text.ToString();
            String phone = textBox2.Text.ToString();
            String e_mail = textBox3.Text.ToString();
            String password = textBox5.Text.ToString();

            bool res = myApi.Insert("users", new String[] { "name", "surname", "phone", "e_mail", "password", "role" },
                          new Object[] { name, surname, phone, e_mail, password, false });
            if (res)
            {
                MessageBox.Show("Вы успешно зарегистрированы! Пожалуйста, выполните вход.");
                User_login_or_administrator frm = new User_login_or_administrator(myApi.GetConnection());
                this.Visible = false;
                frm.Show();

            }
            else
                MessageBox.Show("Данные о новой регистрации содержат ошибки и не могут быть добавлены!");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
