﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Npgsql;
using DbApi;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Buy_ticket_guest : Form
    {
        private MyApi myApi1 = null;

        public Buy_ticket_guest(NpgsqlConnection conn)
        {
            myApi1 = new MyApi();
            myApi1.SetConnection(conn);
            InitializeComponent();
            
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            Guest_menu frm = new Guest_menu(myApi1.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            String name = textBox5.Text.ToString();
            String surname = textBox4.Text.ToString();
            String patronymic = textBox3.Text.ToString();
            String passport = textBox1.Text.ToString();
            var birth_day = dateTimePicker1.Value.Date;
            bool sex;
            String gender = comboBox1.SelectedText.ToString();
            if (gender == "мужской")
                sex = true;
            else
                sex = false;

            String phone = textBox2.Text.ToString();



            bool res = myApi1.Insert("passenger", new String[] { "name", "surname", "patronymic", "passport", "birth_day", "sex", "phone", "count_visit" },
                          new Object[] { name, surname, patronymic, passport, birth_day, sex, phone, 1 });
            if (res)
            {
                MessageBox.Show("Новый пассажир успешно добавлен!");
            }
            else
                MessageBox.Show("Данные о новом пассажире содержат ошибки и не могут быть добавлены!");

            int id_passenger1 = Convert.ToInt32(myApi1.SelectSql("SELECT id_passenger FROM passenger WHERE passport = '" + passport + "'")[0][0]);

            Choose_flight_guest frm = new Choose_flight_guest(myApi1.GetConnection());
            frm.SetIdPassenger(id_passenger1);
            this.Visible = false;
            frm.Show();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
