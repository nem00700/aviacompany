﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Delete_flight : Form
    {
        private MyApi api = null;

        public Delete_flight(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();

            var name_flight = api.Select(new String[] { "flight" }, new String[] { "airport_departure", "airport_arrival"});

            for (int i = 0; i < name_flight.Count; i++)
            {
                comboBox1.Items.Add(name_flight[i][0] + " - " + name_flight[i][1]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name_flight = comboBox1.SelectedItem as String;
            var name_flight_table = api.Select(new String[] { "flight" }, new String[] { "airport_departure", "airport_arrival", "id_flight" });
            String id_flight = "";
            for (int i = 0; i < name_flight_table.Count; i++)
            {
                if (Convert.ToString((name_flight_table[i][0] + " - " + name_flight_table[i][1])) == name_flight)
                {
                    id_flight = Convert.ToString(name_flight_table[i][2]);
                }
                

            }
           

            var res = api.SelectSql("DELETE FROM flight WHERE id_flight = " + id_flight);
            if (true)
            {
                MessageBox.Show("Рейс успешно удален!");
            }
            
        }
    }
}
