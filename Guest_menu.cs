﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Guest_menu : Form
    {
        private MyApi api = null;
        public Guest_menu(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();
        }
        

        private void зарегистрироватьсяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserRegistration frm = new UserRegistration(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void забронироватьБилетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Buy_ticket_guest frm = new Buy_ticket_guest(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            this.Visible = false;
            frm.Show();
        }
    }
}
