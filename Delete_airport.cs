﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using DbApi;
using Npgsql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Delete_airport : Form
    {
        private MyApi api = null;

        public Delete_airport(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();

            var name_airport = api.Select(new String[] { "airport" }, new String[] { "name" });

            for (int i = 0; i < name_airport.Count; i++)
            {
                comboBox1.Items.Add(name_airport[i][0]);
            }
        }
        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name_airport = "'" + comboBox1.SelectedItem as String + "'";
            String id_airport = Convert.ToString(api.SelectSql("SELECT id_airport FROM airport WHERE name = " + name_airport)[0][0]);

            var res = api.SelectSql("DELETE FROM airport WHERE id_airport = " + id_airport);
            if (true)
            {
                MessageBox.Show("Аэропорт успешно удален!");
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
