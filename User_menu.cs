﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class User_menu : Form
    {
        private MyApi api = null;
        public User_menu(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();
        }
        

        private void забронироватьБилетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Buy_ticket_user frm = new Buy_ticket_user(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            this.Visible = false;
            frm.Show();
        }
    }
}
