﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Add_admin : Form
    {
        private MyApi api = null;
        public Add_admin(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();

            var users_table = api.Select(new String[] { "users" }, new String[] { "id_user", "name", "surname", "role" });
            
            for (int i = 0; i < users_table.Count; i++)
            {

                if (users_table[i][3].ToString() == "False")
                    comboBox1.Items.Add(users_table[i][1] + " " + users_table[i][2] + "(" + users_table[i][0] + ")");


            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var users_table = api.Select(new String[] { "users" }, new String[] { "id_user", "name", "surname"});
            String users_box = comboBox1.SelectedItem as String;
            int id_user_admin = 0;
            String name_table;
            String surname_table;
            String id_users_table;
            for (int i = 0; i < users_table.Count; i++)
            {
                name_table = users_table[i][1].ToString();
                surname_table = users_table[i][2].ToString();
                id_users_table = users_table[i][0].ToString();
                if ((name_table + " " + surname_table + "(" + id_users_table + ")").Equals(users_box))
                    id_user_admin = Convert.ToInt32(users_table[i][0]);
            }

            String str_id_user_admin = "'" + id_user_admin + "'"; 

            bool res = api.Update("users", new String[] { "role"}, new String[] { "True"}, "id_user = " + str_id_user_admin);
            if (res)
            {
                MessageBox.Show("Новый администратор успешно добавлен!");
            }
            else
                MessageBox.Show("Данные о новом администраторе содержат ошибки и не могут быть добавлены!");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
