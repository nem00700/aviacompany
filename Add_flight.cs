﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using DbApi;
using Npgsql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Add_flight : Form
    {
        private MyApi myApi1 = null;
        public Add_flight(NpgsqlConnection conn)
        {
            myApi1 = new MyApi();
            myApi1.SetConnection(conn);
            InitializeComponent();

            
            var idAeroports =  myApi1.Select(new String[] { "airport" }, new String[] {"name", "id_airport" });
            
            for (int i = 0; i < idAeroports.Count; i++)
            {
                comboBox1.Items.Add(idAeroports[i][0]);

                comboBox2.Items.Add(idAeroports[i][0]);
            }

          
            var idAirlines = myApi1.Select(new String[] { "airlines" }, new String[] {"name", "id" });

            for (int i = 0; i < idAirlines.Count; i++)
            {
                comboBox3.Items.Add(idAirlines[i][0]);
                
            }

        }
      

        private void button1_Click(object sender, EventArgs e)
        {
            var idAeroports = myApi1.Select(new String[] { "airport" }, new String[] { "name", "id_airport" });
            var idAirlines = myApi1.Select(new String[] { "airlines" }, new String[] { "name", "id" });
            object id_airport_departure = 0;
            object id_airport_arrival = 0;
            object id_aviacompany = 0;
            String airport_departure = comboBox1.SelectedItem as String;
            String airport_arrival = comboBox2.SelectedItem as String;
            String aviacompany = comboBox3.SelectedItem as String;

            String air_dep;
            String air_arr;

            for (int i = 0; i < idAeroports.Count; i++)
            {
                air_dep = idAeroports[i][0].ToString();
                if (airport_departure.Equals(air_dep))
                    id_airport_departure = idAeroports[i][1];
                air_arr = idAeroports[i][0].ToString();
                if (airport_arrival.Equals(air_arr))
                    id_airport_arrival = idAeroports[i][1];

            }

            String aviacom;

            for (int i = 0; i < idAirlines.Count; i++)
            {
                aviacom = idAirlines[i][0].ToString();
                if (aviacompany.Equals(aviacom))
                    id_aviacompany = idAirlines[i][1];
            }


            


            bool res = myApi1.Insert("flight", new String[] { "id_airport_departure", "airport_departure", "id_airport_arrival", "airport_arrival", "id_aviacompany", "aviacompany" },
                          new Object[] {id_airport_departure, airport_departure, id_airport_arrival, airport_arrival, id_aviacompany, aviacompany });
            if (res)
            {
                MessageBox.Show("Новый рейс успешно добавлен!");
            }
            else
                MessageBox.Show("Данные о новом рейсе содержат ошибки и не могут быть добавлены!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(myApi1.GetConnection());
            this.Visible = false;
            frm.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
