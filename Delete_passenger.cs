﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using DbApi;
using Npgsql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Delete_passenger : Form
    {
        private MyApi api = null;

        public Delete_passenger(NpgsqlConnection conn)
        {
            api = new MyApi();
            api.SetConnection(conn);
            InitializeComponent();

            var name_passenger = api.Select(new String[] { "passenger" }, new String[] { "name", "surname", "id_passenger" });

            for (int i = 0; i < name_passenger.Count; i++)
            {
                comboBox1.Items.Add(name_passenger[i][0] + " " + name_passenger[i][1] + "(" + name_passenger[i][2] + ")");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name_passenger = comboBox1.SelectedItem as String;
            var name_passenger_table = api.Select(new String[] { "passenger" }, new String[] { "name", "surname", "id_passenger" });
            String id_passenger = "";
            for (int i = 0; i < name_passenger_table.Count; i++)
            {
                if ((name_passenger_table[i][0] + " " + name_passenger_table[i][1] + "(" + name_passenger_table[i][2] + ")") == name_passenger)
                    id_passenger = Convert.ToString(name_passenger_table[i][2]);

            }

            MessageBox.Show(id_passenger);
            

            var res = api.SelectSql("DELETE FROM passenger WHERE id_passenger = " + id_passenger);
            if (true)
            {
                MessageBox.Show("Пассажир успешно удален!");
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin_menu frm = new Admin_menu(api.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
