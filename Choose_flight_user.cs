﻿using System;
using DbApi;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aviacompany
{
    public partial class Choose_flight_user : Form
    {

        private MyApi myApi1 = null;
        private int id_passenger;
        public Choose_flight_user(NpgsqlConnection conn)
        {
            myApi1 = new MyApi();
            myApi1.SetConnection(conn);
            InitializeComponent();


            var id_and_flight = myApi1.Select(new String[] { "flight" }, new String[] { "id_flight", "airport_departure", "airport_arrival" });

            for (int i = 0; i < id_and_flight.Count; i++)
            {
                comboBox1.Items.Add(id_and_flight[i][1] + " - " + id_and_flight[i][2]);
            }
            

        }

        public void SetIdPassenger(int id_passenger1)
        {
            id_passenger = id_passenger1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            this.Visible = false;
            frm.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    
       

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var id_and_flight = myApi1.Select(new String[] { "flight" }, new String[] { "id_flight", "airport_departure", "airport_arrival" });
            String flight = comboBox1.SelectedItem as String;
            int id_flight = 0;
            String air_dep;
            String air_arr;
            var departure_date = dateTimePicker1.Value.Date;
            for (int i = 0; i < id_and_flight.Count; i++)
            {
                air_dep = id_and_flight[i][1].ToString();
                air_arr = id_and_flight[i][2].ToString();
                if ((air_dep + " - " + air_arr).Equals(flight))
                    id_flight = Convert.ToInt32(id_and_flight[i][0]);
            }

            int id_passenger1 = id_passenger;



            var count = Convert.ToString(myApi1.SelectSql("SELECT count_visit FROM passenger WHERE id_passenger = @id0", new Object[] { id_passenger })[0][0]);



            bool res = myApi1.Insert("ticket", new String[] { "id_flight", "id_passenger", "departure_date" },
                          new Object[] { id_flight, id_passenger1, departure_date });
            if (res)
            {
                MessageBox.Show("Билет успешно зарегистрирован! Количество забронированных билетов с помощью нашего приложения: " + count);
            }
            else
                MessageBox.Show("Данные о новом билете содержат ошибки и не могут быть добавлены!");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            User_menu frm = new User_menu(myApi1.GetConnection());
            this.Visible = false;
            frm.Show();
        }
    }
}
